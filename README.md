# Coding task
## Requirements
Your task is to implement RESTful facade backend API with two mock up services:
- Movie details service (movie: id, title, description)
- Movie comments service (comment: movieId, username, message)

Movie details service and Movie comments service are simple standalone services providing mock up data.

Please do not spend too much effort on implementation of mocked functionality, use libraries available.

Requirements for the facade:

- Endpoint for providing combined movie details and movie comments by movie id (in JSON)
- Dummy endpoint for creating movie details. No communication with Movie details service. Endpoint should be authorized with ROLE_ADMIN
- Dummy endpoint for creating Comment. No communication with Comments service. Endpoint should be authorized with ROLE_USER
- Basic authentication
- No user registration required. You can store mocked user data in memory.
- Calls to the backend services should be implemented asynchronously
- Movie details service responses should be cached with Least Frequently Used strategy
- Comments service responses should also be cached but only as fallback. When the backend service is down, comments should be taken from cache.
- Implemented with JAVA 8
- Should start as a normal java application with embedded servlet container


Things that will be evaluated:

* Implementation (your own code, libraries used, OOP principles)
* Clean code
* REST principles (proper status codes, headers, error responses, etc.)
* Tests
* Commits

## Usage instruction

### Run Facade Service

- Clone Source Code
```
git clone https://korytskyy@bitbucket.org/korytskyy/movie-recruitment-task.git
```

- Build with Maven
```
mvn -f movie-recruitment-task package
```

- Run with Java 8 (ports and urls could be omit, values defined below are default ones)
```
java -jar movie-recruitment-task/target/codingtask-0.0.1-SNAPSHOT.jar --server.port=8080 --movie.provider.url=http://localhost:1234/ --comments.provider.url=http://localhost:4321/
```

### Run Mock Services

- Download Mock Service with Wget
```
wget http://repo1.maven.org/maven2/com/github/tomakehurst/wiremock-standalone/2.19.0/wiremock-standalone-2.19.0.jar
```

- Run Movie Details Mock Service with Java 8
```
java -jar wiremock-standalone-2.19.0.jar --port 1234
```

- Configure Movie Details Mock Service with Curl
```
curl -X POST --data '{ "request": { "url": "/movies/666", "method": "GET" }, "response": { "status": 200, "headers": { "Content-Type": "application/json;charset=UTF-8"}, "body": "{\"id\":666,\"title\":\"666: The Child\",\"description\":\"After a plane crash where a boy is the only survival\"}" }}' http://localhost:1234/__admin/mappings/new
```

- Run Comments Mock Service with Java 8
```
java -jar wiremock-standalone-2.19.0.jar --port 4321
```

- Configure Comments Mock Service with Curl
```
curl -X POST --data '{ "request": { "url": "/movies/666/comments", "method": "GET" }, "response": { "status": 200, "headers": { "Content-Type": "application/json;charset=UTF-8"}, "body": "[{\"movieId\":666,\"username\":\"ifbbfit1\",\"message\":\"it was better than expected\"},{\"movieId\":666,\"username\":\"MrMyron\",\"message\":\"What the hell?????\"}]" }}' http://localhost:4321/__admin/mappings/new
```

### Test

- Request with Curl
```
curl -i http://localhost:8080/movies/666
```

- Expect response
```
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Content-Length: 235
Cache-Control: no-cache, no-store, max-age=0, must-revalidate
Pragma: no-cache
Expires: 0
X-Content-Type-Options: nosniff
X-Frame-Options: DENY
X-XSS-Protection: 1 ; mode=block

{"id":666,"title":"666: The Child","description":"After a plane crash where a boy is the only survival","comments":[{"username":"ifbbfit1","message":"it was better than expected"},{"username":"MrMyron","message":"What the hell?????"}]}
```

## Assumptions during design and development
- third services (mocks) do not require authentication
- endpoint for providing movie details by movie id doesn't require authentication
- dummy endpoints shouldn't update caches
- traffic encryption is out of scope
- API documentation is out of scope
- movie id is positive integer, the rest is string
- comments are not manageable as not having id
- to simplify any error except 404 will be showed with 500 status code without body payload
- 404 will be propagated from mock service without body payload

## Technologies and Design Decision
- Spring Boot 2
    - Spring WebFlux as a reactive framework for nonblocking/asynchronous data aggregation
    - Spring Cache + Caffeine as a cache in-memory cache implementation (could Redis or any other)
    - Spring Security for end point role-based limitations
    - Spring Boot Test as a testing framework
- Wiremock as mock framework for rest services
- AssertJ as assert libraries for testing
- REST API design
    - Movie details service 
        - GET /movies/{id}
            - Request body: no
            - Response codes: 200, 404, any 4xx or 5xx
            - Response 200:
                ```json
                { "id": 1, "title": "a title", "description": "a description" }
                ```
    - Movie comments service
        - GET /movies/{id}/comments
            - Request body: no
            - Response codes: 200, 404, any 4xx or 5xx
            - Response body 200:
                ```json
                [ { "movieId": 1, "username": "a username", "message": "a message" } ]
                ```
    - facade
        - GET /movies/{id}
            - Request body: no
            - Response codes: 201, 400, 404, 500, 502
            - Response body:
                ```json
                {
                  "id": 1,
                  "title": "a title",
                  "description": "a description",
                  "comments": [
                      {
                        "username": "a username",
                        "message": "a message"
                      }
                  ] 
                }
                ```
        - POST /movies/{id}
            - Request body:
                ```json
                { "title": "a title", "description": "a description" }
                ```
            - Response codes: 201, 400, 401, 403, 500
            - Response body for 201:
                ```json
                { "id": 1, "title": "a title", "description": "a description", "comments": [] }
                ```
        - POST /movies/{id}/comments with no body returns JSON payload
            - Request body:
                ```json
                { "username": "a username", "message": "a message" }
                ```
            - Response codes: 201, 400, 401, 403, 500
            - Response body: no (as no id or any new data)

## Todo
- [x] setup reactive spring boot skeleton
- [x] api and use cases design
- [x] end-to-end tests for facade movie details endpoint
    - [x] positive tests: 
        - [x] endpoint returns aggregated data from upstream services
        - [x] endpoint returns movie details with empty comments in case if upstream comments service respond 404
        - [x] endpoint returns cached movie details on second call for the same movie id
        - [x] endpoint returns cached comments on second call for the same movie id when comments service is down, responds too long or return internal error
    - [x] negative tests: 
        - [x] endpoint returns error 502 when movie details service is unreachable, return 5xx status code or not answering
        - [x] endpoint returns error 404 when movie details service returns 404
        - [x] endpoint returns error 400 when passed id is not integer
- [x] end-to-end tests for facade create movie endpoint
    - [x] positive tests:
        - [x] endpoint returns 201 with payload for authenticated admin user
    - [x] negative tests: 
        - [x] endpoint returns 400 for authenticated admin user but invalid json payload
        - [x] endpoint returns 401 for not authenticated client
        - [x] endpoint returns 403 for not admin user
- [x] end-to-end tests for facade create comment endpoint
    - [x] positive tests:
        - [x] endpoint returns 201 for authenticated user 
    - [x] negative tests: 
        - [x] endpoint returns 400 for authenticated user but invalid json payload
        - [x] endpoint returns 401 for not authenticated client
        - [x] endpoint returns 403 for authenticated client but not user