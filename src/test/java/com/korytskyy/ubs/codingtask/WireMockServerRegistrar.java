package com.korytskyy.ubs.codingtask;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ConfigurableApplicationContext;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

public class WireMockServerRegistrar {
    private final static Logger logger = LoggerFactory.getLogger(WireMockServerRegistrar.class);
    
    public static void registerWireMockServers(ConfigurableApplicationContext configurableApplicationContext, String... names) {
        for (String name : names) {
            WireMockServer movieProvider = startWireMockServerOnRandomPort(name);
            TestPropertyValues.of(name + ".server.port=" + movieProvider.port()).applyTo(configurableApplicationContext);
            configurableApplicationContext.getBeanFactory().registerSingleton(name, movieProvider);
        }
    }
    
    private static WireMockServer startWireMockServerOnRandomPort(String name) {
        WireMockServer wireMockServer = new WireMockServer(wireMockConfig().dynamicPort());
        wireMockServer.start();
        logger.info("Started WireMockServer {} on port {}", name, wireMockServer.port());
        return wireMockServer;
    }
}
