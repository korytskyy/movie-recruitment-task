package com.korytskyy.ubs.codingtask.e2e;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.korytskyy.ubs.codingtask.WireMockServerRegistrar;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import javax.annotation.Resource;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = CommentsUpstreamDownCacheE2ETest.CustomInitializer.class)
@TestPropertySource(locations = "classpath:test.properties")
public class CommentsUpstreamDownCacheE2ETest {
    
    @Resource
    private WebTestClient client;
    
    @Resource
    private WireMockServer movieProvider;
    
    @Resource
    private WireMockServer commentsProvider;
    
    @Resource
    private CacheManager cacheManager;
    
    @Before
    public void setUp() {
        cacheManager.getCache("movieDetails").clear();
        cacheManager.getCache("comments").clear();
    }
    
    @Test
    public void getMovieShouldReturnCachedCommentsWhenCommentsUpstreamIsDown() {
        //given
        movieProvider.givenThat(get(urlEqualTo("/movies/1"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\"}")));
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("[ { \"movieId\": 1, \"username\": \"a username\", \"message\": \"a message\" }]")));
        //when then
        client.get().uri("/movies/1").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody().json("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\", " +
                "\"comments\": [ { \"username\": \"a username\", \"message\": \"a message\" }]}");
        
        commentsProvider.stop();
        
        client.get().uri("/movies/1").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody().json("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\", " +
                "\"comments\": [ { \"username\": \"a username\", \"message\": \"a message\" }]}");
    }
    
    public static class CustomInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            WireMockServerRegistrar.registerWireMockServers(configurableApplicationContext, "movieProvider", "commentsProvider");
        }
    }
}
