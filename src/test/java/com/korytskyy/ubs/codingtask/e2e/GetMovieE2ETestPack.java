package com.korytskyy.ubs.codingtask.e2e;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.korytskyy.ubs.codingtask.WireMockServerRegistrar;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import javax.annotation.Resource;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static wiremock.org.apache.http.HttpStatus.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = GetMovieE2ETestPack.CustomInitializer.class)
@TestPropertySource(locations = "classpath:test.properties")
public class GetMovieE2ETestPack {
    
    @Resource
    private WebTestClient client;
    
    @Resource
    private WireMockServer movieProvider;
    
    @Resource
    private WireMockServer commentsProvider;
    
    @Value("${movie.provider.timeout}")
    private int movieProviderTimeout;
    
    @Value("${comments.provider.timeout}")
    private int commentsProviderTimeout;
    
    @Resource
    private CacheManager cacheManager;
    
    @Before
    public void setUp() {
        movieProvider.resetAll();
        commentsProvider.resetAll();
        cacheManager.getCache("movieDetails").clear();
        cacheManager.getCache("comments").clear();
    }
    
    @Test
    public void getMovieShouldReturnAggregatedDataFromLivingServices() {
        //given
        movieProvider.givenThat(get(urlEqualTo("/movies/1"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\"}")));
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("[ { \"movieId\": 1, \"username\": \"a username\", \"message\": \"a message\" }, " +
                                "{ \"movieId\": 1, \"username\": \"a username2\", \"message\": \"a message2\" }]")));
    
        //when then
        client.get().uri("/movies/1").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody().json("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\", " +
                "\"comments\": [ { \"username\": \"a username\", \"message\": \"a message\" }," +
                "{ \"username\": \"a username2\", \"message\": \"a message2\" }]}");
    }
    
    @Test
    public void getMovieShouldReturnEmptyCommentsIfUpstreamCommentsServiceRespondNotFound() {
        //given
        movieProvider.givenThat(get(urlEqualTo("/movies/1"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\"}")));
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse().withStatus(SC_NOT_FOUND)));
        
        //when then
        client.get().uri("/movies/1").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody().json("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\", \"comments\": []}");
    }
    
    @Test
    public void getMovieShouldReturnBadGatewayInCaseOfMovieDetailsUpstreamReturnsInternalError() {
        //given
        movieProvider.givenThat(get(urlEqualTo("/movies/1"))
                .willReturn(aResponse().withStatus(SC_INTERNAL_SERVER_ERROR)));
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("[]")));
        
        //when then
        client.get().uri("/movies/1").exchange().expectStatus().isEqualTo(BAD_GATEWAY);
    }
    
    @Test
    public void getMovieShouldReturnBadGatewayInCaseOfMovieDetailsUpstreamsTooLongResponse() {
        //given
        movieProvider.givenThat(get(urlEqualTo("/movies/1"))
                .willReturn(aResponse().withFixedDelay(movieProviderTimeout + 1).withStatus(SC_OK)));
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("[]")));
        
        //when then
        client.get().uri("/movies/1").exchange().expectStatus().isEqualTo(BAD_GATEWAY);
    }
    
    @Test
    public void getMovieShouldReturnNotFoundInCaseOfMovieDetailsUpstreamsReturnsNotFound() {
        //given
        movieProvider.givenThat(get(urlEqualTo("/movies/1"))
                .willReturn(aResponse().withStatus(SC_NOT_FOUND)));
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("[]")));
        
        //when then
        client.get().uri("/movies/1").exchange().expectStatus().isEqualTo(NOT_FOUND);
    }
    
    @Test
    public void getMovieShouldReturnBadRequestInCaseOfNonIntegerMovieIdRequest() {
        //given when then
        client.get().uri("/movies/one").exchange().expectStatus().isEqualTo(BAD_REQUEST);
    }
    
    @Test
    public void getMovieShouldReturnCachedMovieDetailsAfterSecondCall() {
        //given
        movieProvider.givenThat(get(urlEqualTo("/movies/1"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\"}")));
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("[]")));
        
        //when then
        client.get().uri("/movies/1").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody().json("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\", \"comments\": []}");
        
        client.get().uri("/movies/1").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody().json("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\", \"comments\": []}");
        
        movieProvider.verify(1, getRequestedFor(urlEqualTo("/movies/1")));
    }
    
    @Test
    public void getMovieShouldReturnBadGatewayWhenCommentsUpstreamRespondsInternalErrorAndNoCache() {
        //given
        movieProvider.givenThat(get(urlEqualTo("/movies/1"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\"}")));
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse().withStatus(SC_INTERNAL_SERVER_ERROR)));
        //when then
        client.get().uri("/movies/1").exchange().expectStatus().isEqualTo(BAD_GATEWAY);
    }
    
    @Test
    public void getMovieShouldReturnCachedCommentsWhenCommentsUpstreamRespondsInternalErrorButThereIsCache() {
        //given
        movieProvider.givenThat(get(urlEqualTo("/movies/1"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\"}")));
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("[ { \"movieId\": 1, \"username\": \"a username\", \"message\": \"a message\" }]")));
        //when then
        client.get().uri("/movies/1").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody().json("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\", " +
                "\"comments\": [ { \"username\": \"a username\", \"message\": \"a message\" }]}");
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse().withStatus(SC_INTERNAL_SERVER_ERROR)));
        
        client.get().uri("/movies/1").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody().json("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\", " +
                "\"comments\": [ { \"username\": \"a username\", \"message\": \"a message\" }]}");
    }
    
    @Test
    public void getMovieShouldReturnCachedCommentsWhenCommentsUpstreamRespondsTooLong() {
        //given
        movieProvider.givenThat(get(urlEqualTo("/movies/1"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\"}")));
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("[ { \"movieId\": 1, \"username\": \"a username\", \"message\": \"a message\" }]")));
        //when then
        client.get().uri("/movies/1").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody().json("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\", " +
                "\"comments\": [ { \"username\": \"a username\", \"message\": \"a message\" }]}");
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse().withFixedDelay(commentsProviderTimeout + 1).withStatus(SC_OK)));
        
        client.get().uri("/movies/1").exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(APPLICATION_JSON_UTF8)
                .expectBody().json("{\"id\": 1,\"title\": \"a title\",\"description\": \"a description\", " +
                "\"comments\": [ { \"username\": \"a username\", \"message\": \"a message\" }]}");
    }
    
    public static class CustomInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            WireMockServerRegistrar.registerWireMockServers(configurableApplicationContext, "movieProvider", "commentsProvider");
        }
    }
}
