package com.korytskyy.ubs.codingtask.e2e;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.korytskyy.ubs.codingtask.WireMockServerRegistrar;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import javax.annotation.Resource;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.BAD_GATEWAY;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = MovieUpstreamDownCacheE2ETest.CustomInitializer.class)
@TestPropertySource(locations = "classpath:test.properties")
public class MovieUpstreamDownCacheE2ETest {
    
    @Resource
    private WebTestClient client;
    
    @Resource
    private WireMockServer movieProvider;
    
    @Resource
    private WireMockServer commentsProvider;
    
    @Resource
    private CacheManager cacheManager;
    
    @Before
    public void setUp() {
        movieProvider.resetAll();
        commentsProvider.resetAll();
        cacheManager.getCache("movieDetails").clear();
        cacheManager.getCache("comments").clear();
    }
    
    @Test
    public void getMovieShouldReturnBadGatewayInCaseOfMovieDetailsUpstreamDowntime() {
        //given
        movieProvider.stop();
        
        commentsProvider.givenThat(get(urlEqualTo("/movies/1/comments"))
                .willReturn(aResponse()
                        .withHeader(CONTENT_TYPE, "application/json;charset=UTF-8")
                        .withBody("[ { \"movieId\": 1, \"username\": \"a username\", \"message\": \"a message\" } ]")));
        
        //when then
        client.get().uri("/movies/1").exchange()
                .expectStatus().isEqualTo(BAD_GATEWAY);
    }
    
    public static class CustomInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            WireMockServerRegistrar.registerWireMockServers(configurableApplicationContext, "movieProvider", "commentsProvider");
        }
    }
}
