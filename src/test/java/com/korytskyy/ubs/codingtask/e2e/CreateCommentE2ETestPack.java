package com.korytskyy.ubs.codingtask.e2e;

import com.korytskyy.ubs.codingtask.WireMockServerRegistrar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = CreateCommentE2ETestPack.CustomInitializer.class)
@TestPropertySource(locations = "classpath:test.properties")
public class CreateCommentE2ETestPack {
    
    @Resource
    private WebTestClient client;
    
    @Test
    public void createCommentShouldReturnUnauthorizedCodeWhenClientCallEndpointWithoutAuthentication() {
        client.post().uri("/movies/1/comments").exchange().expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED);
    }
    
    @Test
    public void createCommentShouldReturnForbiddenCodeForNotUser() {
        client.mutate().filter(ExchangeFilterFunctions.basicAuthentication("admin", "admin")).build()
                .post().uri("/movies/1/comments").exchange()
                .expectStatus().isEqualTo(FORBIDDEN);
    }
    
    @Test
    public void createCommentShouldReturnBadRequestCodeForWrongPayload() {
        client.mutate().filter(ExchangeFilterFunctions.basicAuthentication("user", "user")).build()
                .post().uri("/movies/1/comments")
                .contentType(APPLICATION_JSON_UTF8)
                .body(Mono.just("{\"wrong\":\"json\"}"), String.class)
                .exchange()
                .expectStatus().isEqualTo(BAD_REQUEST);
    }
    
    @Test
    public void createCommentShouldReturnMoviePayloadWithId() {
        client.mutate().filter(ExchangeFilterFunctions.basicAuthentication("user", "user")).build()
                .post().uri("/movies/1/comments")
                .contentType(APPLICATION_JSON_UTF8)
                .body(Mono.just("{\"username\":\"user\",\"message\":\"hi\"}"), String.class)
                .exchange()
                .expectStatus().isOk();
    }
    
    public static class CustomInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            WireMockServerRegistrar.registerWireMockServers(configurableApplicationContext, "movieProvider", "commentsProvider");
        }
    }
}
