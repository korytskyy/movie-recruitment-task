package com.korytskyy.ubs.codingtask.e2e;

import com.korytskyy.ubs.codingtask.WireMockServerRegistrar;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = CreateMovieE2ETestPack.CustomInitializer.class)
@TestPropertySource(locations = "classpath:test.properties")
public class CreateMovieE2ETestPack {
    
    @Resource
    private WebTestClient client;
    
    @Test
    public void createMovieShouldReturnUnauthorizedCodeWhenClientCallEndpointWithoutAuthentication() {
        client.post().uri("/movies").exchange().expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED);
    }
    
    @Test
    public void createMovieShouldReturnForbiddenCodeForNotAdmin() {
        client.mutate().filter(ExchangeFilterFunctions.basicAuthentication("user", "user")).build()
                .post().uri("/movies").exchange()
                .expectStatus().isEqualTo(FORBIDDEN);
    }
    
    @Test
    public void createMovieShouldReturnBadRequestCodeForWrongPayload() {
        client.mutate().filter(ExchangeFilterFunctions.basicAuthentication("admin", "admin")).build()
                .post().uri("/movies")
                .contentType(APPLICATION_JSON_UTF8)
                .body(Mono.just("{\"wrong\":\"json\"}"), String.class)
                .exchange()
                .expectStatus().isEqualTo(BAD_REQUEST);
    }
    
    @Test
    public void createMovieShouldReturnMoviePayloadWithId() {
        client.mutate().filter(ExchangeFilterFunctions.basicAuthentication("admin", "admin")).build()
                .post().uri("/movies")
                .contentType(APPLICATION_JSON_UTF8)
                .body(Mono.just("{\"title\":\"new\",\"description\":\"desc\"}"), String.class)
                .exchange()
                .expectStatus().isOk()
                .expectBody().jsonPath("id").isNumber()
                .jsonPath("title").isEqualTo("new")
                .jsonPath("description").isEqualTo("desc");
    }
    
    public static class CustomInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            WireMockServerRegistrar.registerWireMockServers(configurableApplicationContext, "movieProvider", "commentsProvider");
        }
    }
}
