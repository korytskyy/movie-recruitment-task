package com.korytskyy.ubs.codingtask.config;

import com.korytskyy.ubs.codingtask.api.upstream.comments.CacheableCommentsProviderRestClient;
import com.korytskyy.ubs.codingtask.api.upstream.comments.CommentsProviderClient;
import com.korytskyy.ubs.codingtask.api.upstream.comments.CommentsProviderRestClient;
import com.korytskyy.ubs.codingtask.api.upstream.movie.CacheableMovieProviderRestClient;
import com.korytskyy.ubs.codingtask.api.upstream.movie.MovieProviderClient;
import com.korytskyy.ubs.codingtask.api.upstream.movie.MovieProviderRestClient;
import com.korytskyy.ubs.codingtask.controller.MovieRequestHandler;
import com.korytskyy.ubs.codingtask.service.MovieService;
import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static com.korytskyy.ubs.codingtask.controller.MovieRequestHandler.MOVIE_ID;
import static java.lang.String.format;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@EnableCaching
public class ApplicationContextConfiguration {
    
    @Resource
    private CacheManager cacheManager;
    
    @Bean
    public RouterFunction<ServerResponse> routes(MovieRequestHandler movieRequestHandler) {
        return route(GET(format("/movies/{%s}", MOVIE_ID)), movieRequestHandler::getMovie).and(
                route(POST("/movies"), movieRequestHandler::createMovie)).and(
                route(POST(format("/movies/{%s}/comments", MOVIE_ID)), movieRequestHandler::createComment));
    }
    
    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        return http
                .csrf().disable()
                .authorizeExchange()
                .pathMatchers(POST, "/movies").hasRole("ADMIN")
                .pathMatchers(POST, "/movies/*/comments").hasRole("USER")
                .pathMatchers(GET, "/movies/*").permitAll()
                .and().httpBasic()
                .and().build();
    }
    
    @Bean
    public MapReactiveUserDetailsService userDetailsService() {
        return new MapReactiveUserDetailsService(
                User.withUsername("user").passwordEncoder(defaultPasswordEncoder()).password("user").roles("USER").build(),
                User.withUsername("admin").passwordEncoder(defaultPasswordEncoder()).password("admin").roles("ADMIN").build());
    }
    
    private Function<String, String> defaultPasswordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode;
    }
    
    @Bean
    public MovieRequestHandler movieRequestHandler(MovieService movieService) {
        return new MovieRequestHandler(movieService);
    }
    
    @Bean
    public MovieService movieService(MovieProviderClient cacheableMovieProviderClient, CommentsProviderClient cacheableCommentsProviderClient) {
        return new MovieService(cacheableMovieProviderClient, cacheableCommentsProviderClient);
    }
    
    @Bean
    public MovieProviderClient cacheableMovieProviderClient(MovieProviderClient movieProviderClient) {
        return new CacheableMovieProviderRestClient(movieProviderClient, cacheManager.getCache("movieDetails"));
    }
    
    @Bean
    public MovieProviderClient movieProviderClient(@Value("${movie.provider.url}") String movieProviderUrl, @Value("${movie.provider.timeout}") int timeout) {
        return new MovieProviderRestClient(buildWebClient(movieProviderUrl, timeout));
    }
    
    @Bean
    CommentsProviderClient cacheableCommentsProviderClient(CommentsProviderClient commentsProviderClient) {
        return new CacheableCommentsProviderRestClient(commentsProviderClient, cacheManager.getCache("comments"));
    }
    
    @Bean
    CommentsProviderClient commentsProviderClient(@Value("${comments.provider.url}") String commentsProviderUrl, @Value("${comments.provider.timeout}") int timeout) {
        return new CommentsProviderRestClient(buildWebClient(commentsProviderUrl, timeout));
    }
    
    private WebClient buildWebClient(@Value("${movie.provider.url}") String movieProviderUrl, final int timeout) {
        ReactorClientHttpConnector clientHttpConnector = new ReactorClientHttpConnector(builder ->
                builder.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, timeout)
                        .afterNettyContextInit(ctx -> ctx.addHandlerLast(new ReadTimeoutHandler(timeout, TimeUnit.MILLISECONDS))));
        return WebClient.builder().clientConnector(clientHttpConnector).baseUrl(movieProviderUrl).build();
    }
}
