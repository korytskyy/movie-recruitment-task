package com.korytskyy.ubs.codingtask.controller;

import com.korytskyy.ubs.codingtask.api.upstream.MovieNotFoundException;
import com.korytskyy.ubs.codingtask.api.upstream.UpstreamCommunicationException;
import com.korytskyy.ubs.codingtask.api.upstream.UpstreamServiceException;
import com.korytskyy.ubs.codingtask.model.Comment;
import com.korytskyy.ubs.codingtask.model.Movie;
import com.korytskyy.ubs.codingtask.model.request.CreateCommentRequest;
import com.korytskyy.ubs.codingtask.model.request.CreateMovieRequest;
import com.korytskyy.ubs.codingtask.service.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.concurrent.ThreadLocalRandom;

import static java.util.Collections.emptyList;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;
import static org.springframework.web.reactive.function.server.ServerResponse.*;

public class MovieRequestHandler {
    public static final String MOVIE_ID = "movieId";
    private final MovieService movieService;
    
    public MovieRequestHandler(MovieService movieService) {
        this.movieService = movieService;
    }
    
    public Mono<ServerResponse> getMovie(ServerRequest request) {
        try {
            long movieId = Long.parseLong(request.pathVariable(MOVIE_ID));
            return movieService.findMovie(movieId)
                    .flatMap(movie -> ok().contentType(APPLICATION_JSON_UTF8).body(fromObject(movie)))
                    .onErrorResume(e -> e instanceof UpstreamServiceException || e instanceof UpstreamCommunicationException,
                            e -> status(HttpStatus.BAD_GATEWAY).build())
                    .onErrorResume(e -> e instanceof MovieNotFoundException,
                            e -> status(HttpStatus.NOT_FOUND).build());
        } catch (NumberFormatException e) {
            return badRequest().build();
        }
    }
    
    public Mono<ServerResponse> createMovie(ServerRequest request) {
        return request.bodyToMono(CreateMovieRequest.class)
                .map(r -> new Movie(ThreadLocalRandom.current().nextLong(100500), r.getTitle(), r.getDescription(), emptyList()))
                .flatMap(m -> ok().contentType(APPLICATION_JSON_UTF8).body(fromObject(m)))
                .onErrorResume(e -> e instanceof NullPointerException, e -> badRequest().build());
    }
    
    public Mono<ServerResponse> createComment(ServerRequest request) {
        try {
            Long.parseLong(request.pathVariable(MOVIE_ID));
            return request.bodyToMono(CreateCommentRequest.class)
                    .map(r -> new Comment(r.getUsername(), r.getMessage()))
                    .flatMap(c -> ok().build())
                    .onErrorResume(e -> e instanceof NullPointerException, e -> badRequest().build());
        } catch (NumberFormatException e) {
            return badRequest().build();
        }
    }
}
