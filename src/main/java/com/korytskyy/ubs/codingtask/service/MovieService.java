package com.korytskyy.ubs.codingtask.service;

import com.korytskyy.ubs.codingtask.api.upstream.comments.CommentDetails;
import com.korytskyy.ubs.codingtask.api.upstream.comments.CommentsProviderClient;
import com.korytskyy.ubs.codingtask.api.upstream.movie.MovieDetails;
import com.korytskyy.ubs.codingtask.api.upstream.movie.MovieProviderClient;
import com.korytskyy.ubs.codingtask.model.Comment;
import com.korytskyy.ubs.codingtask.model.Movie;
import reactor.core.publisher.Mono;

import java.util.List;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

public class MovieService {
    private final MovieProviderClient movieProviderClient;
    private final CommentsProviderClient commentsProviderClient;
    
    public MovieService(MovieProviderClient movieProviderClient, CommentsProviderClient commentsProviderClient) {
        this.movieProviderClient = requireNonNull(movieProviderClient);
        this.commentsProviderClient = requireNonNull(commentsProviderClient);
    }
    
    public Mono<Movie> findMovie(long movieId) {
        return movieProviderClient.getMovie(movieId)
                .zipWith(commentsProviderClient.getComments(movieId))
                .map(o -> aggregateMovie(o.getT1(), o.getT2()));
    }
    
    private Movie aggregateMovie(MovieDetails movieDetails, List<CommentDetails> commentDetails) {
        return new Movie(movieDetails.getId(), movieDetails.getTitle(), movieDetails.getDescription(),
                commentDetails.stream().map(c -> new Comment(c.getUsername(), c.getMessage())).collect(toList()));
    }
}
