package com.korytskyy.ubs.codingtask.model;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class Comment {
    private final String username;
    private final String message;
    
    public Comment(String username, String message) {
        this.username = requireNonNull(username);
        this.message = requireNonNull(message);
    }
    
    public String getUsername() {
        return username;
    }
    
    public String getMessage() {
        return message;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return Objects.equals(username, comment.username) &&
                Objects.equals(message, comment.message);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(username, message);
    }
}
