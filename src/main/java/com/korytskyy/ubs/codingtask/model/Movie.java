package com.korytskyy.ubs.codingtask.model;

import java.util.List;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class Movie {
    private final long id;
    private final String title;
    private final String description;
    private final List<Comment> comments;
    
    public Movie(long id, String title, String description, List<Comment> comments) {
        this.id = id;
        this.title = requireNonNull(title);
        this.description = requireNonNull(description);
        this.comments = requireNonNull(comments);
    }
    
    public long getId() {
        return id;
    }
    
    public String getTitle() {
        return title;
    }
    
    public String getDescription() {
        return description;
    }
    
    public List<Comment> getComments() {
        return comments;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return id == movie.id &&
                Objects.equals(title, movie.title) &&
                Objects.equals(description, movie.description) &&
                Objects.equals(comments, movie.comments);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, comments);
    }
}
