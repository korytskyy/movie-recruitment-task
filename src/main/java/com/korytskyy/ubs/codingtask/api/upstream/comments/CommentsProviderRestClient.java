package com.korytskyy.ubs.codingtask.api.upstream.comments;

import com.korytskyy.ubs.codingtask.api.upstream.CommentsNotFoundException;
import com.korytskyy.ubs.codingtask.api.upstream.UpstreamServiceException;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

public class CommentsProviderRestClient implements CommentsProviderClient {
    private final WebClient webClient;
    
    public CommentsProviderRestClient(WebClient webClient) {
        this.webClient = webClient;
    }
    
    @Override
    public Mono<List<CommentDetails>> getComments(long movieId) {
        return webClient.get().uri("/movies/{movieId}/comments", movieId).retrieve()
                .onStatus(HttpStatus::is5xxServerError, r -> Mono.error(new UpstreamServiceException()))
                .onStatus(status -> status == HttpStatus.NOT_FOUND, r -> Mono.error(new CommentsNotFoundException()))
                .bodyToMono(new ParameterizedTypeReference<List<CommentDetails>>() {
        });
    }
}
