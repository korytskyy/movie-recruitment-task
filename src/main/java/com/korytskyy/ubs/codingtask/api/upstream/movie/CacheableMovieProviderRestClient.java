package com.korytskyy.ubs.codingtask.api.upstream.movie;

import org.springframework.cache.Cache;
import reactor.cache.CacheMono;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Signal;

import static java.util.Objects.requireNonNull;

public class CacheableMovieProviderRestClient implements MovieProviderClient {
    private final MovieProviderClient movieProviderClient;
    private final Cache cache;
    
    public CacheableMovieProviderRestClient(MovieProviderClient movieProviderClient, Cache cache) {
        this.movieProviderClient = requireNonNull(movieProviderClient);
        this.cache = requireNonNull(cache);
    }
    
    @Override
    public Mono<MovieDetails> getMovie(long id) {
        return CacheMono.lookup(movieId -> Mono.justOrEmpty(cache.get(movieId, MovieDetails.class)).map(Signal::next), id)
                .onCacheMissResume(() -> movieProviderClient.getMovie(id))
                .andWriteWith((movieId, signal) -> Mono.fromRunnable(() -> cache.put(movieId, signal.get())));
    }
}
