package com.korytskyy.ubs.codingtask.api.upstream.movie;

import com.korytskyy.ubs.codingtask.api.upstream.MovieNotFoundException;
import com.korytskyy.ubs.codingtask.api.upstream.UpstreamCommunicationException;
import com.korytskyy.ubs.codingtask.api.upstream.UpstreamServiceException;
import io.netty.handler.timeout.ReadTimeoutException;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.net.ConnectException;

public class MovieProviderRestClient implements MovieProviderClient {
    
    private final WebClient webClient;
    
    public MovieProviderRestClient(WebClient webClient) {
        this.webClient = webClient;
    }
    
    @Override
    public Mono<MovieDetails> getMovie(long id) {
        return webClient.get().uri("/movies/{id}", id).retrieve()
                .onStatus(HttpStatus::is5xxServerError, response -> Mono.error(new UpstreamServiceException()))
                .onStatus(status -> status == HttpStatus.NOT_FOUND, response -> Mono.error(new MovieNotFoundException()))
                .bodyToMono(MovieDetails.class)
                .onErrorResume(e -> e instanceof ConnectException || e instanceof ReadTimeoutException, e -> Mono.error(new UpstreamCommunicationException()));
    }
    
}
