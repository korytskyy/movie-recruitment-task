package com.korytskyy.ubs.codingtask.api.upstream.comments;

import reactor.core.publisher.Mono;

import java.util.List;

public interface CommentsProviderClient {
    Mono<List<CommentDetails>> getComments(long movieId);
}
