package com.korytskyy.ubs.codingtask.api.upstream.movie;

import reactor.core.publisher.Mono;

public interface MovieProviderClient {
    Mono<MovieDetails> getMovie(long id);
}
