package com.korytskyy.ubs.codingtask.api.upstream.comments;

import com.korytskyy.ubs.codingtask.api.upstream.CommentsNotFoundException;
import com.korytskyy.ubs.codingtask.api.upstream.UpstreamServiceException;
import io.netty.handler.timeout.ReadTimeoutException;
import org.springframework.cache.Cache;
import reactor.cache.CacheMono;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Signal;

import java.net.ConnectException;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class CacheableCommentsProviderRestClient implements CommentsProviderClient {
    private final CommentsProviderClient commentsProviderClient;
    private final Cache cache;
    
    public CacheableCommentsProviderRestClient(CommentsProviderClient commentsProviderClient, Cache cache) {
        this.commentsProviderClient = requireNonNull(commentsProviderClient);
        this.cache = requireNonNull(cache);
    }
    
    @Override
    
    public Mono<List<CommentDetails>> getComments(long movieId) {
        return CacheMono.lookup(id -> Mono.<List<CommentDetails>>empty().map(Signal::next), movieId)
                .onCacheMissResume(() ->
                        commentsProviderClient.getComments(movieId)
                                .onErrorResume(e -> e instanceof CommentsNotFoundException, e -> Mono.just(Collections.emptyList()))
                                .onErrorResume(e -> e instanceof ConnectException
                                                || e instanceof UpstreamServiceException
                                                || e instanceof ReadTimeoutException,
                                        e -> Mono.justOrEmpty(getFromCache(movieId)).switchIfEmpty(Mono.error(e))))
                .andWriteWith((id, signal) -> Mono.fromRunnable(() -> cache.put(id, signal.get())));
    }
    
    @SuppressWarnings("unchecked")
    private List<CommentDetails> getFromCache(long movieId) {
        Cache.ValueWrapper valueWrapper = cache.get(movieId);
        return valueWrapper != null ? (List<CommentDetails>) valueWrapper.get() : null;
    }
}
